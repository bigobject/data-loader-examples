import json
import sys
import csv
import os

class OUTPUTError(Exception):
      def __init__(self,value):
            self.value=value
      def __str__(self):
            return repr(self.value)


def createOutputDir(outputdir):
        try:
           os.makedirs(outputdir)
        except:
           pass
            
def parseDir(dirname,tid,outputdir):
        filelist=os.listdir(dirname)
        print filelist
        datalist=[]
        for name in filelist:
            absname=os.path.join(dirname,name)
            absoutname=os.path.join(outputdir,name+".csv")
            if os.path.isfile(absname):
                  tid+=parseFile(absname,tid,absoutname)
                  
            else:
               print absname+" is not a file"


def parseFile(filename,tid,outputname):
        print "start to parse the file: " + filename
        jsondata=list()
        try:
            with open(filename,'rU') as f:
               for line in f:
                    jsondata.append(json.loads(line))
        except:
            print "failed to load file content"

        basename=os.path.basename(filename)
        date=basename[:basename.find(".json")]
        listdata=[]
        for line in jsondata:
            if "user_id" in line:
                 user_id=line["user_id"]
                 if "hashtags" in line:
                      for tag in line["hashtags"]:
                           row=[]
                           row.extend([line["user_id"],unicode(tag).encode("utf-8"),"%0.12d"%(tid),date])
                           tid+=1
                           listdata.append(row)
                 else:
                       print "hashtags is not found"
            else:
                 print "user_id is not found" 
        
        writeCSV(listdata,outputname)
        return len(listdata)

def writeCSV(datalist,outputname):
     with open(outputname,"w") as f:
          writer=csv.writer(f)
          for row in datalist:
               writer.writerow(row)
     
     print "output to "+outputname
 
if __name__=="__main__":
     inputname=sys.argv[1]
     outputname=sys.argv[2] if len(sys.argv)>=3 else "./result"
     createOutputDir(outputname)
     if os.path.isfile(inputname):
             print "type:file detected"
             parseFile(inputname,0,os.path.join(outputname,inputname+".csv"))
     elif os.path.isdir(inputname):
             print "type:dir detected"
             parseDir(inputname,0,outputname)
     else:
         print "No such files or directories"
